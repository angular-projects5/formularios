import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaisService } from 'src/app/services/pais.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {

  usuario = {
    nombre: 'David',
    apellido: 'Marchante',
    correo: 'davidmarchantecano@gmail.com',
    pais: 'ESP',
    genero: 'M'
  };

  paises: any[] = [];

  constructor(private paisService: PaisService) { }

  ngOnInit(): void {

    this.paisService.getPaises()
      .subscribe(paises => {
        console.log(paises);
        this.paises = paises;
        this.paises.unshift({
          nombre: '[Seleccione Pais]',
          codigo: ''
        });
      });
  }

  guardar(forma: NgForm) {
    console.log(forma);
    console.log(forma.value);

    if (forma.invalid) {
      Object.values(forma.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }
  }

}
