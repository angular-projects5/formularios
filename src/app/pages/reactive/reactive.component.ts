import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma: FormGroup;

  constructor(private fb: FormBuilder,
    private validadores: ValidadoresService) {
    // Aquí es donde se aconseja crear el formulario
    this.crearFormulario();
    this.cargarDatosFormulario();
    this.crearListeners();
  }

  ngOnInit(): void {
  }

  get pasatiempos() {
    return this.forma.get('pasatiempos') as FormArray;
  }

  get nombreNoValido() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched;
  }

  get apellidoNoValido() {
    return this.forma.get('apellido').invalid && this.forma.get('apellido').touched;
  }

  get correoNoValido() {
    return this.forma.get('correo').invalid && this.forma.get('correo').touched;
  }

  get usuarioNoValido() {
    return this.forma.get('usuario').invalid && this.forma.get('usuario').touched;
  }

  get distritoNoValido() {
    return this.forma.get('direccion.distrito').invalid && this.forma.get('direccion.distrito').touched;
  }

  get ciudadNoValida() {
    return this.forma.get('direccion.ciudad').invalid && this.forma.get('direccion.ciudad').touched;
  }
  get pass1NoValido() {
    return this.forma.get('pass1').invalid && this.forma.get('pass1').touched;
  }
  get pass2NoValido() {
    const pass1 = this.forma.get('pass1').value;
    const pass2 = this.forma.get('pass2').value;
    return pass1 !== pass2;
  }

  private crearFormulario() {
    // Existen validators como nullValidator, pattern, minLength (para caracteres), min (para números)
    // la validación del email no pide un . algo.. mejor usar por pattern
    // Los parámetros de cada group son [valor, validadoresSincronos, validadoresAsincronos]
    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      // En los errores, sale noChante de error
      apellido: ['', [Validators.required, this.validadores.noApellidoChante]],
      correo: ['@gmail.com', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      // Validación asíncrona
      usuario: ['', , this.validadores.existeUsuario],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      direccion: this.fb.group({
        distrito: ['', Validators.required],
        ciudad: ['', Validators.required]
      }),
      // para arrays en el form
      pasatiempos: this.fb.array([])

    },
      // Para añadir validaciones que influyan a ambos campos del formulario
      {
        validators: [this.validadores.passwordsIguales('pass1', 'pass2')]
      });
  }

  crearListeners() {
    // Para observar cualquier cambio en los valores del formulario
    this.forma.valueChanges
      .subscribe(valor => {
        console.log(valor);
      });
    // Para observar cualquier cambio en los estados del formulario
    this.forma.statusChanges
      .subscribe((status) => {
        console.log(status);
      });
    // Para escuchar únicamente un cambio del formulario.
    this.forma.get('nombre').valueChanges
      .subscribe(console.log);
  }

  guardar() {
    console.log(this.forma);

    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach(control => {

        if (control instanceof FormGroup) {
          Object.values(control.controls).forEach(cntrl => { cntrl.markAsTouched(); });
        }

        control.markAsTouched();
      });
    }

    // Posteo de la información

    // Resetear el formulario
    // this.forma.reset(); o
    this.forma.reset({
      nombre: 'Sin nombre'
    });
  }

  cargarDatosFormulario() {
    // this.forma.setValue({
    //   nombre: 'David',
    //   apellido: 'Marchante'
    // });

    // Con setValue hay que dar todo el objeto, con reset das sólo los que quieres e ignora el resto
    // this.forma.setValue({
    //   nombre: 'David',
    //   apellido: 'Marchante',
    //   correo: 'davidmarchantecano@gmail.com',
    //   direccion: {
    //     distrito: 'Getafe',
    //     ciudad: 'Madrid'
    //   }
    // });

    this.forma.reset({
      nombre: 'David',
      apellido: 'Marchante',
      correo: 'davidmarchantecano@gmail.com',
      pass1: '123',
      pass2: '123',
      direccion: {
        distrito: 'Getafe',
        ciudad: 'Madrid'
      }
    });

    // Para inicializar el form array no se puede hacer en el reset, hay que hacerlo en el setValue (dando valor a todos los elementos)
    // o del array que queremos como a continuación

    ['Jugar', 'Ejercicio'].forEach(valor => this.pasatiempos.push(this.fb.control(valor)));
  }


  agregarPasatiempo() {
    this.pasatiempos.push(this.fb.control('Nuevo elemento', Validators.required));
  }

  borrarPasatiempo(index: number) {
    this.pasatiempos.removeAt(index);
  }

}
