import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

interface ErrorValidate {
  [s: string]: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor() { }

  noApellidoChante(control: FormControl): { [s: string]: boolean } {
    if (control.value?.toLowerCase() === 'chante') {
      return { noChante: true };
    }
    return null;
  }

  passwordsIguales(name1: string, name2: string) {
    // Como la validación se hace a nivel de formulario, recibimos un formgroup
    return (formGroup: FormGroup) => {
      const pass1Control = formGroup.controls[name1];
      const pass2Control = formGroup.controls[name2];

      const error = pass1Control.value === pass2Control.value ? null : { noEsIgual: true };

      pass2Control.setErrors(error);
    };
  }

  existeUsuario(control: FormControl): Promise<ErrorValidate> | Observable<ErrorValidate> {
    if (!control.value) { return Promise.resolve(null); }
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'david') {
          resolve({ existe: true });
        } else {
          resolve(null);
        }
      }, 3500);
    });
  }
}
